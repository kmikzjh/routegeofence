#polygon
puntos = []
puntos.append(Point(-73.07556, -36.80819))
puntos.append(Point(-73.06801, -36.81423))
puntos.append(Point(-73.06664, -36.82083))
puntos.append(Point(-73.05771, -36.83402))
puntos.append(Point(-73.04810, -36.85545))
puntos.append(Point(-73.04398, -36.87028))
puntos.append(Point(-73.01308, -36.84336))
puntos.append(Point(-73.00278, -36.83677))
puntos.append(Point(-73.00758, -36.82083))
puntos.append(Point(-73.03024, -36.81368))
puntos.append(Point(-73.04672, -36.82138))
puntos.append(Point(-73.04672, -36.81423))
puntos.append(Point(-73.05702, -36.80104))

#markers
markers = []
markers.append(Point(-73.02784, -36.84226))
markers.append(Point(-72.96467, -36.84116))
markers.append(Point(-73.02235, -36.86863))
markers.append(Point(-73.07625, -36.84666))


#Ejemplo
polygon = Polygon(puntos)
for marker in markers:
    print polygon.FindPoint(marker.X, marker.Y)
