# -*- coding: utf-8 -*-

# A simple geo fencing using polygon method (transcripcion)
# http://www.codeproject.com/Articles/62482/A-Simple-Geo-Fencing-Using-Polygon-Method

class Point:
    def __init__(self, X, Y):
        self.X = X
        self.Y = Y

class Polygon:
    def __init__(self,points):
        self.Points = points
    def Add(self,point):
        self.Points.append(point)
    def Count(self):
        return len(self.Points)
    """
    The function will return true if the point x,y is inside the polygon, or
    false if it is not.  If the point is exactly on the edge of the polygon,
    then the function may return true or false.
    """
    def FindPoint(self,X,Y):
        sides = self.Count()-1
        j = sides - 1
        pointStatus = False
        ec1 = 0
        for i in range(sides):
            if (self.Points[i].Y < Y and self.Points[j].Y >= Y) or \
                (self.Points[j].Y < Y and self.Points[i].Y >= Y):
                if self.Points[i].X + (Y - self.Points[i].Y) / \
                    (self.Points[j].Y - self.Points[i].Y) * \
                    (self.Points[j].X - self.Points[i].X) < X:
                    pointStatus = not pointStatus
            j = i
        return pointStatus

